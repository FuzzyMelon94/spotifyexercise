'''
Created on 3 Sep. 2017

@author: Tom
'''

import sqlite3

class AcmeDatabase(object):
    '''The class for controlling/simulating the Acme database'''
    
    def __init__(self, database_directory):
        self.database_directory = database_directory
    
    def connect(self):
        '''Connect to the specified database.'''
        
        self.conn = sqlite3.connect(self.database_directory)
    
    
    def close(self):
        '''Close the connection to the database.'''
        
        self.conn.close()
        
    
    def commit(self):
        '''Commit changes to the database.'''
        
        self.conn.commit()
        
    
    def drop_table(self, table_name):
        '''Drop the specified table from the database, if it exists.'''
        
        query_string = "DROP TABLE IF EXISTS {}".format(table_name)
        self.conn.execute(query_string)
    
    
    def create_table(self, table_name):
        '''Create the specified table in the database, if it doesn't already exist.'''
        
        query_string = """CREATE TABLE IF NOT EXISTS {}
                         (ID            TEXT  PRIMARY KEY  NOT NULL,
                          NAME  TEXT    NOT NULL,
                          TYPE  TEXT    NOT NULL,
                          DATE  INT     NOT NULL);""".format(table_name)
        self.conn.execute(query_string)
    
    
    def setup_new_releases(self):
        '''Setup the new releases table in the database.'''
        
        self.connect()
        self.create_table("new_releases")
        self.close()
        
        
    def reset_new_releases(self):
        '''Remove the new releases table in the database, then create a new one.'''
        
        self.connect()
        self.drop_table("new_releases")
        self.setup_new_releases()
        self.close()
    
    
    def add_new_releases(self, release_id, release_name, release_type, release_date):
        '''Add the specified entry to the new releases table of the database.'''
        
        self.connect()
        
        query_string = "SELECT EXISTS (SELECT 1 FROM new_releases WHERE ID='{}')".format(release_id)
        cursor = self.conn.execute(query_string)
        
        # If the entry already exists, based on album ID, update the fields, otherwise insert it
        if cursor.fetchone()[0]:
            query_string = "UPDATE new_releases SET NAME='{}', TYPE='{}', DATE='{}' \
                            WHERE ID='{}'".format(release_name, release_type, release_date, release_id)
            self.conn.execute(query_string)
        else:        
            self.conn.execute("INSERT INTO new_releases (ID, NAME, TYPE, DATE) \
                               VALUES (?, ?, ?, ?)", (release_id, release_name, release_type, release_date))
        
        self.commit()
        self.close()
        
        
    def find_new_release(self, find_id):
        '''Returns the ID, Name, Type, and Release Date of the specified release.'''
        
        self.connect()
        
        query_string = "SELECT * FROM new_releases WHERE id='{}'".format(find_id)
        cursor = self.conn.execute(query_string)
        release_info = cursor.fetchone()
        
        self.close()
        
        if release_info:
            return {"id": release_info[0],
                    "name": release_info[1],
                    "type": release_info[2],
                    "date": release_info[3]}
        else:
            return {"id": "false"}
        