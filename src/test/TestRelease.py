'''
Created on 8 Sep. 2017

@author: Tom
'''
import unittest
from Release import Release

class Test(unittest.TestCase):
    album_id = "album123"
    album_name = "Unidentified"
    album_type = "single"
    artist_id = "artist567"
    artist_name = "John Doe"
    spotify_popularity = "100"
    release_date = "2017-09-08"
    rating = "Not rated"
    new_rating = "1"


    def setUp(self):
        self.release = Release(self.album_id, self.album_name, self.album_type,
                               self.artist_id, self.artist_name, self.spotify_popularity,
                               self.release_date, self.rating)
    

    def test_release_album_id(self):
        info = self.release.get_release_information("album id")
        self.assertEqual(info, self.album_id, "Album IDs don't match.")
        
    def test_release_album_name(self):
        info = self.release.get_release_information("album name")
        self.assertEqual(info, self.album_name, "Album Names don't match.")
        
    def test_release_album_type(self):
        info = self.release.get_release_information("album type")
        self.assertEqual(info, self.album_type, "Album Types don't match.")
        
    def test_release_artist_id(self):
        info = self.release.get_release_information("artist id")
        self.assertEqual(info, self.artist_id, "Artist IDs don't match.")
        
    def test_release_artist_name(self):
        info = self.release.get_release_information("artist name")
        self.assertEqual(info, self.artist_name, "Artist Names don't match.")
        
    def test_release_spotify_popularity(self):
        info = self.release.get_release_information("spotify popularity")
        self.assertEqual(info, self.spotify_popularity, "Spotify Popularities don't match.")

    def test_release_release_date(self):
        info = self.release.get_release_information("release date")
        self.assertEqual(info, self.release_date, "Release Dates don't match.")
    
    def test_release_rating(self):
        info = self.release.get_release_information("rating")
        self.assertEqual(info, self.rating, "Ratings don't match.")

    def test_update_rating(self):
        self.release.update_release_rating(self.new_rating)
        info = self.release.get_release_information("rating")
        self.assertEqual(info, self.new_rating, "Ratings don't match.")


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()