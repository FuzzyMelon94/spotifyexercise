'''
Created on 8 Sep. 2017

@author: Tom
'''

import json
import unittest
from os.path import exists
from AcmeDatabase import AcmeDatabase
from GUI import GUI
from Release import Release


class Test(unittest.TestCase):

    test_gui = GUI(False, "test_database.db")
    
    test_string_json = {
            "albums": {
                "items": [{"album_type": "single",
                           "artists": [{"id": "06HL4z0CvFAxyc27GXpf02",
                                        "name": "Taylor Swift"}],
                           "id": "0HG8fMDhvN2tH5uPHFsyZP",
                           "name": "...Ready For It?"},
                          {"album_type": "album",
                           "artists": [{"id": "6TQj5BFPooTa08A7pk8AQ1",
                                        "name": "Kaskade"}],
                           "id": "1wy9Wrfx1mjBREqJEsXUTP",
                           "name": "Redux EP 002"}]
                       }
                   }   


    def test_add_to_database(self):
        '''Add the test releases to the list_of_releases, add the releases to the database,
           make sure the data is correct when brought back from the database.'''
        
        test_release_00 = Release("xyz789", 
                                  "My Amazing Album",
                                  "Album",
                                  "abc123",
                                  "John Doe",
                                  "76",
                                  "2017-09-08")
        test_release_01 = Release("qwerty123", 
                                  "Just a Single",
                                  "Single",
                                  "uiop0987",
                                  "Alex Smith",
                                  "56",
                                  "2017-09-08")
        
        self.test_gui.list_of_releases.clear()
        self.test_gui.list_of_releases.append(test_release_00)
        self.test_gui.list_of_releases.append(test_release_01)
        
        self.test_gui.reset_database()
        self.test_gui.add_releases_to_database()
        
        find_release = self.test_gui.acme_db.find_new_release("qwerty123")
        
        self.assertEqual(find_release["id"], "qwerty123",
                         "ID from the database does not match the Album ID of the release.")
        self.assertEqual(find_release["name"], "Just a Single",
                         "Name from the database does not match the Album Name of the release.")
        self.assertEqual(find_release["type"], "Single",
                         "Type from the database does not match the Album Type of the release.")
        self.assertEqual(find_release["date"], "2017-09-08",
                         "Date from the database does not match the Release Date of the release.")
        
        pass
    
    
    def test_get_new_releases(self):
        '''Set new_releases_raw to None, then get the new releases to check it works.'''
        
        self.test_gui.new_releases_raw = None
        self.test_gui.get_new_releases()
        self.assertNotEqual(self.test_gui.new_releases_raw, None, "Could not get the new releases.")
    
    
    def test_new_releases_to_list(self):
        '''Using some test JSON, make sure releases are created and populated correctly.'''
        
        self.test_gui.new_releases_raw = self.test_string_json
        self.test_gui.new_releases_to_list()
        
        album_name_00 = self.test_gui.list_of_releases[0].get_release_information("album name")
        album_id_00 = self.test_gui.list_of_releases[0].get_release_information("album id")
        
        artist_name_01 = self.test_gui.list_of_releases[1].get_release_information("artist name")
        artist_id_01 = self.test_gui.list_of_releases[1].get_release_information("artist id")
        
        self.assertEqual(album_name_00, "...Ready For It?", "Album names for item 0 do not match")
        self.assertEqual(album_id_00, "0HG8fMDhvN2tH5uPHFsyZP", "Album IDs for item 0 do not match")
        self.assertEqual(artist_name_01, "Kaskade", "Artist names for item 0 do not match")
        self.assertEqual(artist_id_01, "6TQj5BFPooTa08A7pk8AQ1", "Artist names for item 0 do not match")
    
    
    def test_open_ratings_file_correct_file_path(self):
        '''Simulates opening a CSV file in the GUI, the CSV should be parsed.'''
        
        self.test_gui.csv_albums.clear()
        self.test_gui.csv_artists.clear()
        self.test_gui.csv_ratings.clear()
        
        self.test_gui.open_ratings_file("test_ratings.csv")
        
        # Using "1" because "0" contains the headings of the CSV file
        album_01 = self.test_gui.csv_albums[1]
        artist_01 = self.test_gui.csv_artists[1]
        rating_01 = self.test_gui.csv_ratings[1]
        
        self.assertEqual(album_01, "...Ready For It?", "csv_albums[1] is not correctly set.")
        self.assertEqual(artist_01, "Taylor Swift", "csv_artists[1] is not correctly set.")
        self.assertEqual(rating_01, "3", "csv_ratings[1] is not correctly set.")
    

    def test_open_ratings_file_blank_path(self):
        '''Simulates pressing the "Cancel" button on the file browser dialog of the GUI.
           The function should return before trying to open a file, no error.'''
        
        self.test_gui.csv_albums.clear()
        self.test_gui.csv_artists.clear()
        self.test_gui.csv_ratings.clear()
        
        self.test_gui.open_ratings_file("")
        
        # Using "1" because "0" contains the headings of the CSV file
        album_01_length = len(self.test_gui.csv_albums)
        artist_01_length = len(self.test_gui.csv_artists)
        rating_01_length = len(self.test_gui.csv_ratings)
        
        self.assertEqual(album_01_length, 0, "csv_albums should be empty.")
        self.assertEqual(artist_01_length, 0, "csv_artists should be empty.")
        self.assertEqual(rating_01_length, 0, "csv_ratings should be empty.")    
    
    
    def test_save_new_releases_json_correct_file_path(self):
        '''Using test JSON content, save the JSON, then open the file and check the contents.'''
        
        self.test_gui.new_releases_raw = self.test_string_json
        file_album_id = ""
        file_album_name = ""
        file_artist_id = ""
        file_artist_name = ""
        
        self.test_gui.save_new_releases_json("test_json.json")
        
        if exists("test_json.json"):
            with open("test_json.json", "r") as open_file:
                read_file = json.load(open_file)
                
                file_album_id = read_file["albums"]["items"][0]["id"]
                file_album_name = read_file["albums"]["items"][0]["name"]
                file_artist_id = read_file["albums"]["items"][0]["artists"][0]["id"]
                file_artist_name = read_file["albums"]["items"][0]["artists"][0]["name"]
        
        self.assertEqual(file_album_id, "0HG8fMDhvN2tH5uPHFsyZP",
                         "The Album ID read from the file does not match the new_releases_raw data.")
        self.assertEqual(file_album_name, "...Ready For It?",
                         "The Album Name read from the file does not match the new_releases_raw data.")
        self.assertEqual(file_artist_id, "06HL4z0CvFAxyc27GXpf02",
                         "The Artist ID read from the file does not match the new_releases_raw data.")
        self.assertEqual(file_artist_name, "Taylor Swift",
                         "The Artist Name read from the file does not match the new_releases_raw data.")


    def test_save_new_releases_json_blank_file_path(self):
        '''Simulates the "Cancel" button being pressed in the "Save as..." window of the GUI.'''
        
        self.test_gui.new_releases_raw = self.test_string_json
        file_album_id = ""
        file_album_name = ""
        file_artist_id = ""
        file_artist_name = ""
        
        self.test_gui.save_new_releases_json("")
        
        self.assertEqual(file_album_id, "", "The Album ID should remain blank.")
        self.assertEqual(file_album_name, "", "The Album Name should remain blank.")
        self.assertEqual(file_artist_id, "", "The Artist ID should remain blank.")
        self.assertEqual(file_artist_name, "", "The Artist Name should remain blank.")
    
    
    def test_update_release_ratings_single_matching(self):
        '''Setup a single test release.
           Add matching data to the csv lists - an update should occur.
           Call the update rating method and make sure the rating is updated.'''
                   
        new_rating = "80"
        test_release_00 = Release("xyz789", 
                                  "My Amazing Album",
                                  "Album",
                                  "abc123",
                                  "John Doe",
                                  "76",
                                  "2017-09-08")
                
        self.test_gui.list_of_releases.clear()
        self.test_gui.list_of_releases.append(test_release_00)
        
        self.test_gui.csv_albums.clear()
        self.test_gui.csv_artists.clear()
        self.test_gui.csv_ratings.clear()
        
        self.test_gui.csv_albums.append("My Amazing Album")
        self.test_gui.csv_artists.append("John Doe")
        self.test_gui.csv_ratings.append(new_rating)
        
        self.test_gui.update_release_ratings()
        release_rating = self.test_gui.list_of_releases[0].get_release_information("rating")
        self.assertEqual(release_rating, new_rating, "Ratings do not match.")
    
    
    def test_update_release_ratings_single_nonmatching(self):
        '''Setup a single test release.
           Add non-matching data to the csv lists - no update should occur.
           Call the update rating method and make sure the rating is updated.'''
        
        new_rating = "80"
        test_release_00 = Release("xyz789", 
                                  "My Amazing Album",
                                  "Album",
                                  "abc123",
                                  "John Doe",
                                  "76",
                                  "2017-09-08")
        
        self.test_gui.list_of_releases.clear()
        self.test_gui.list_of_releases.append(test_release_00)
        
        self.test_gui.csv_albums.clear()
        self.test_gui.csv_artists.clear()
        self.test_gui.csv_ratings.clear()
        
        self.test_gui.csv_albums.append("Album Number 2")
        self.test_gui.csv_artists.append("Alex Smith")
        self.test_gui.csv_ratings.append(new_rating)
        
        self.test_gui.update_release_ratings()
        release_rating = self.test_gui.list_of_releases[0].get_release_information("rating")
        self.assertNotEqual(release_rating, new_rating, "Ratings should not match but they do.")


    def test_update_release_ratings_mutiple_one_match(self):
        '''Setup two test releases with different data.
           Add data to the csv lists - the second release should be updated.
           Call the update rating method and make sure the rating is updated.'''
        
        new_rating = "80"
        
        test_release_00 = Release("xyz789", 
                                  "My Amazing Album",
                                  "Album",
                                  "abc123",
                                  "John Doe",
                                  "76",
                                  "2017-09-08")
        test_release_01 = Release("qwerty123", 
                                  "Just a Single",
                                  "Single",
                                  "uiop0987",
                                  "Alex Smith",
                                  "56",
                                  "2017-09-08")
        
        self.test_gui.list_of_releases.clear()
        self.test_gui.list_of_releases.append(test_release_00)
        self.test_gui.list_of_releases.append(test_release_01)
        
        self.test_gui.csv_albums.clear()
        self.test_gui.csv_artists.clear()
        self.test_gui.csv_ratings.clear()
        
        self.test_gui.csv_albums.append("Just a Single")
        self.test_gui.csv_artists.append("Alex Smith")
        self.test_gui.csv_ratings.append(new_rating)
        
        self.test_gui.update_release_ratings()
        release_rating_00 = self.test_gui.list_of_releases[0].get_release_information("rating")
        release_rating_01 = self.test_gui.list_of_releases[1].get_release_information("rating")
        self.assertNotEqual(release_rating_00, new_rating, "Ratings should not match but they do.")
        self.assertEqual(release_rating_01, new_rating, "Ratings should match but they do not.")


    def test_update_release_ratings_mutiple_duplicate_one_match(self):
        '''Setup two test releases with the same album names.
           Add data to the csv lists - the second release should be updated.
           Call the update rating method and make sure the rating is updated.'''
            
        new_rating = "80"
        test_release_00 = Release("xyz789", 
                                  "My Amazing Album",
                                  "Album",
                                  "abc123",
                                  "John Doe",
                                  "76",
                                  "2017-09-08")
        test_release_01 = Release("qwerty123", 
                                  "My Amazing Album",
                                  "Single",
                                  "uiop0987",
                                  "Alex Smith",
                                  "56",
                                  "2017-09-08")
        
        self.test_gui.list_of_releases.clear()
        self.test_gui.list_of_releases.append(test_release_00)
        self.test_gui.list_of_releases.append(test_release_01)
        
        self.test_gui.csv_albums.clear()
        self.test_gui.csv_artists.clear()
        self.test_gui.csv_ratings.clear()
        
        self.test_gui.csv_albums.append("My Amazing Album")
        self.test_gui.csv_artists.append("Alex Smith")
        self.test_gui.csv_ratings.append(new_rating)
        
        self.test_gui.update_release_ratings()
        release_rating_00 = self.test_gui.list_of_releases[0].get_release_information("rating")
        release_rating_01 = self.test_gui.list_of_releases[1].get_release_information("rating")
        self.assertNotEqual(release_rating_00, new_rating, "Ratings should not match but they do.")
        self.assertEqual(release_rating_01, new_rating, "Ratings should match but they do not.")
    
    
    def test_view_newer_releases_start_zero(self):
        '''Offset starts at 0, display limit is 5. Should return 0.'''

        self.test_gui.new_release_offset.set(0)
        self.test_gui.release_display_limit.set(5)
        self.test_gui.view_newer_releases()
        end_value = self.test_gui.new_release_offset.get()
        self.assertEqual(end_value, 0, 
                         "The end value should be 0, the offset should not drop below 0.")
    
    
    def test_view_newer_releases_start_below_five(self):
        '''Offset starts at 3, display limit is 5. Should return 0.'''
        
        self.test_gui.new_release_offset.set(3)
        self.test_gui.release_display_limit.set(5)
        self.test_gui.view_newer_releases()
        end_value = self.test_gui.new_release_offset.get()
        self.assertEqual(end_value, 0, 
                         "The end value should be 0, the offset should not drop below 0.")
        
    
    def test_view_newer_releases_start_five(self):
        '''Offset starts at 5, display limit is 5. Should return 0.'''
        
        self.test_gui.new_release_offset.set(5)
        self.test_gui.release_display_limit.set(5)
        self.test_gui.view_newer_releases()
        end_value = self.test_gui.new_release_offset.get()
        self.assertEqual(end_value, 0, 
                         "The offset has not decreased by the display limit amount.")
        
    
    def test_view_newer_releases_start_above_five(self):
        '''Offset starts at 7, display limit is 5. Should return 2.'''
        
        self.test_gui.new_release_offset.set(7)
        self.test_gui.release_display_limit.set(5)
        self.test_gui.view_newer_releases()
        end_value = self.test_gui.new_release_offset.get()
        self.assertEqual(end_value, 2, 
                         "The offset has not decreased by the display limit amount.")
    
    
    def test_view_older_releases(self):
        '''The offset should increase by 5.'''
        
        self.test_gui.new_release_offset.set(0)
        self.test_gui.release_display_limit.set(5)
        start_value = self.test_gui.new_release_offset.get()
        display_limit = self.test_gui.release_display_limit.get()
        self.test_gui.view_older_releases()
        end_value = self.test_gui.new_release_offset.get()
        self.assertEqual(end_value, start_value + display_limit, 
                         "The offset has not increased by the display limit amount.")
    

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()