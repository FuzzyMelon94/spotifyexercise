'''
Created on 4 Sep. 2017

@author: Tom
'''


class Release(object):
    '''A basic class to store information about each release.'''
    
    def __init__(self, album_id, album_name, album_type, artist_id, artist_name,
                 spotify_popularity, release_date, rating="Not rated"):
        self.album_id = album_id
        self.album_name = album_name
        self.album_type = album_type
        self.artist_id = artist_id
        self.artist_name = artist_name
        self.spotify_popularity = spotify_popularity
        self.release_date = release_date
        self.rating = rating
        
    
    def get_release_information(self, info_field):
        '''Returns a dictionary containing information about this release.'''
        
        release_info = {"album id": self.album_id,
                        "album name": self.album_name,
                        "album type": self.album_type,
                        "artist id": self.artist_id,
                        "artist name": self.artist_name,
                        "spotify popularity": self.spotify_popularity,
                        "release date": self.release_date,
                        "rating": self.rating}
        
        return release_info[info_field]
    
    
    def update_release_rating(self, new_value):
        '''Updates the rating field for this release.'''
        
        self.rating = new_value