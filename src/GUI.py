'''
Created on 7 Sep. 2017

@author: Tom
'''

import csv
import json
import tkinter
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
from AcmeDatabase import AcmeDatabase
import Release
import Spotify

class GUI(object):   
    '''Set up and control the GUI for this application.'''
    
    def __init__(self, show_gui=True, database_directory="database.db"):             
        # Widget sizing
        self.frame_right_padx = 8
        self.frame_right_pady = 2
        self.label_name_width = 15
        self.label_value_width = 65
        self.padding_release_horizontal = 8
        self.release_list_button_padx_outer = 220
        self.release_list_button_padx_inner = 20
        self.release_list_button_width = 15
        self.release_list_button_height = 2
        self.release_spacer_height = 5
        self.right_heading_padding_top = 5
        self.right_heading_padding_bottom = 5
        self.right_button_area_padding_vertical = 15
        self.spacer_width = 2

        # Colours
        self.col_left_top_bottom = "#cfcfcf"
        self.col_left_middle = "#f8f8f8"
        self.col_release_info_light = "#f2f2f2"
        self.col_release_info_dark = "#e6e6e6"
        self.col_right_heading = "#b6b6b6"
        self.col_right = "#fdfdfd"
        self.col_spacer = "#b6b6b6"
        
        # Lists
        self.csv_albums = []
        self.csv_artists = []
        self.csv_ratings = []
        
        # Other variables
        self.show_gui = show_gui
        
        # Prepare the initial window
        self.window_root = tkinter.Tk()
        self.window_root.title("New Releases on Spotify")
        self.window_root.resizable(False, False)
        
        # Initialise any variables requiring a root window (tkinter specific)
        self.release_display_limit = tkinter.IntVar()
        self.release_display_limit.set(5)
        self.new_release_offset = tkinter.IntVar()
        self.new_release_offset.set(0)
        
        # Prepare main GUI
        self.prepare_left_frames()
        self.prepare_right_frames() # Still need to add the buttons
        
        # Connect to Spotify
        self.spotify = Spotify.Spotify()
        self.list_of_releases = []
        self.new_releases_raw = ""
        self.get_new_releases()
        
        # Connect to the database
        self.acme_db = AcmeDatabase(database_directory)
        self.acme_db.setup_new_releases()
        
        # If the GUI should not be shown, hide the window        
        if show_gui == False:
            self.window_root.destroy()
        
        # Display the GUI
        self.window_root.mainloop()
        
        # Make sure the application quits when the window is closed
        try:
            self.window_root.destroy()
        except:
            "Do nothing"

        
    def prepare_left_frames(self):
        '''Prepare the frames on the left hand side of the window of the application.'''
        
        # Left frame root
        self.frame_left = tkinter.Frame(self.window_root)
        self.frame_left.pack(side=tkinter.LEFT)
        
        # Left frame middle content area
        self.frame_left_middle = tkinter.Frame(self.frame_left,
                                               width=300,
                                               height=500,
                                               bg=self.col_left_middle)
        self.frame_left_middle.pack(fill=tkinter.BOTH)
        
    
    def prepare_right_frames(self):
        '''Prepare the frames on the right hand side of the window of the application.'''
        
        # Spacing column left
        self.frame_spacer = tkinter.Frame(self.window_root,
                                          width=self.spacer_width,
                                          height=100,
                                          bg=self.col_spacer,
                                          bd=1)
        self.frame_spacer.pack(side=tkinter.LEFT,
                               fill=tkinter.Y)
        
        # Right frames
        self.frame_right = tkinter.Frame(self.window_root,
                                         bg=self.col_right)
        self.frame_right.pack(side=tkinter.LEFT,
                              fill=tkinter.Y)
        
        # Right 'new releases' heading frame
        self.frame_right_new_releases_heading = tkinter.Frame(self.frame_right,
                                                              bg=self.col_right)
        self.frame_right_new_releases_heading.pack(fill=tkinter.X)
        
        self.label_right_new_releases_heading = tkinter.Label(self.frame_right_new_releases_heading,
                                                              text="New Releases",
                                                              bg=self.col_right_heading)
        self.label_right_new_releases_heading.pack(ipady=self.right_heading_padding_bottom,
                                                   pady=(0, self.right_heading_padding_top),
                                                   fill=tkinter.X)
        
        # Right 'new releases' buttons frame    
        self.frame_right_new_releases = tkinter.Frame(self.frame_right,
                                                      bg=self.col_right)
        self.frame_right_new_releases.pack(fill=tkinter.X,
                                           ipady=self.right_button_area_padding_vertical)
        
        self.frame_offset_input = tkinter.Frame(self.frame_right_new_releases,
                                                bg=self.col_right)
        self.frame_offset_input.pack(fill=tkinter.X, 
                                     padx=self.frame_right_padx,
                                     pady=(0, self.frame_right_pady))
        
        self.label_offset = tkinter.Label(self.frame_offset_input,
                                          text="Start at release number",
                                          bg=self.col_right)
        self.label_offset.pack(side=tkinter.LEFT)
        
        self.input_offset_field = tkinter.ttk.Entry(self.frame_offset_input,
                                                    width=5,
                                                    textvariable=self.new_release_offset, 
                                                    justify=tkinter.CENTER)
        self.input_offset_field.pack(side=tkinter.RIGHT)
        
        self.button_list_new_releases = tkinter.ttk.Button(self.frame_right_new_releases,
                                                           text="List new releases", 
                                                           command=lambda: self.get_new_releases())
        self.button_list_new_releases.pack(fill=tkinter.X,
                                           padx=self.frame_right_padx,
                                           pady=self.frame_right_pady)
        
        self.button_view_newer = tkinter.ttk.Button(self.frame_right_new_releases,
                                                    text=("View "
                                                          + str(self.release_display_limit.get()) 
                                                          + " newer"), 
                                                    command=lambda: self.view_newer_releases(),
                                                    state=tkinter.DISABLED)
        self.button_view_newer.pack(fill=tkinter.X,
                                    padx=self.frame_right_padx,
                                    pady=self.frame_right_pady)
        
        self.button_view_older = tkinter.ttk.Button(self.frame_right_new_releases,
                                                    text=("View "
                                                          + str(self.release_display_limit.get()) 
                                                          + " older"), 
                                                    command=lambda: self.view_older_releases())
        self.button_view_older.pack(fill=tkinter.X,
                                    padx=self.frame_right_padx,
                                    pady=self.frame_right_pady) 
        
        # Right 'database' heading frame
        self.frame_right_database_heading = tkinter.Frame(self.frame_right,
                                                          bg=self.col_right)
        self.frame_right_database_heading.pack(fill=tkinter.X)
        
        self.label_right_database_heading = tkinter.Label(self.frame_right_database_heading,
                                                          text="Database",
                                                          bg=self.col_right_heading)
        self.label_right_database_heading.pack(ipady=self.right_heading_padding_bottom,
                                               pady=(0, self.right_heading_padding_top),
                                               fill=tkinter.X)
         
        # Right 'database' buttons frame
        self.frame_right_database = tkinter.Frame(self.frame_right,
                                                  bg=self.col_right)
        self.frame_right_database.pack(fill=tkinter.X,
                                       ipady=self.right_button_area_padding_vertical)
        
        self.add_to_database = tkinter.ttk.Button(self.frame_right_database,
                                                  text="Sync with database", 
                                                  command=lambda: self.add_releases_to_database())  
        self.add_to_database.pack(fill=tkinter.X,
                                  padx=self.frame_right_padx,
                                  pady=self.frame_right_pady)
         
        self.button_reset_database = tkinter.ttk.Button(self.frame_right_database,
                                                 text="Reset database", 
                                                 command=lambda: self.reset_database())
        self.button_reset_database.pack(fill=tkinter.X,
                                 padx=self.frame_right_padx,
                                 pady=self.frame_right_pady)
         
        # Right 'files' heading frame
        self.frame_right_files_heading = tkinter.Frame(self.frame_right,
                                                       bg=self.col_right)
        self.frame_right_files_heading.pack(fill=tkinter.X)
        
        self.label_right_files_heading = tkinter.Label(self.frame_right_files_heading,
                                               text="Files",
                                               bg=self.col_right_heading)
        self.label_right_files_heading.pack(ipady=self.right_heading_padding_bottom,
                                            pady=(0, self.right_heading_padding_top),
                                            fill=tkinter.X)
        
        # Right 'files' buttons frame
        self.frame_right_files = tkinter.Frame(self.frame_right,
                                               bg=self.col_right)
        self.frame_right_files.pack(fill=tkinter.X,
                                    ipady=self.right_button_area_padding_vertical)
        
        self.button_load_ratings = tkinter.ttk.Button(self.frame_right_files,
                                               text="Load ratings file", 
                                               command=lambda: self.open_ratings_file())
        self.button_load_ratings.pack(fill=tkinter.X,
                                      padx=self.frame_right_padx,
                                      pady=self.frame_right_pady)
        
        self.save_as_json = tkinter.ttk.Button(self.frame_right_files,
                                               text="Save raw JSON file", 
                                               command=lambda: self.save_new_releases_json())
        self.save_as_json.pack(fill=tkinter.X,
                               padx=self.frame_right_padx,
                               pady=self.frame_right_pady)
        
        # Spacing column right
        self.frame_spacer = tkinter.Frame(self.window_root,
                                          width=self.spacer_width,
                                          height=100,
                                          bg=self.col_spacer, 
                                          bd=1)
        self.frame_spacer.pack(side=tkinter.LEFT,
                               fill=tkinter.Y)
    
    
    def add_releases_to_database(self):    
        '''Add the current list of releases to the database.'''
            
        if self.list_of_releases == None:
            return
        
        for release in self.list_of_releases:
            self.acme_db.add_new_releases(release.get_release_information("album id"), 
                                          release.get_release_information("album name"), 
                                          release.get_release_information("album type"),
                                          release.get_release_information("release date"))
    
    
    def display_releases(self):
        '''Display the current list of releases in the GUI.'''
        
        if self.list_of_releases == None:
            return
        
        if self.show_gui == False:
            return
        
        if len(self.list_of_releases) == 0:
            self.no_releases_message = tkinter.Label(self.frame_left_middle, 
                                                     text="Sorry, no releases yet. Try clicking 'List new releases'.")
            self.no_releases_message.pack()
        else:
            for child in self.frame_left_middle.winfo_children():
                child.destroy()
            
            counter = 0
            # For each releases, set up a new row in the GUI for the information
            for release in self.list_of_releases:
                # Alternate the background colours for readability
                if counter % 2 == 0:
                    col_background = self.col_release_info_light
                else:
                    col_background = self.col_release_info_dark
                
                # Add a spacer before the release
                self.frame_release_spacer = tkinter.Frame(self.frame_left_middle,
                                                          height=self.release_spacer_height,
                                                          bg=col_background)
                self.frame_release_spacer.pack(fill=tkinter.X)
                
                # Add a frame for the release
                self.frame_release = tkinter.Frame(self.frame_left_middle,
                                                   bg=col_background)
                self.frame_release.pack(fill=tkinter.X)
                
                # Format the information for this release
                #self.format_release_information(release, counter, "Album ID", "album id")
                self.format_release_information(release, counter, "Album Name", "album name")
                self.format_release_information(release, counter, "Album Type", "album type")
                #self.format_release_information(release, counter, "Artist ID", "artist id")
                self.format_release_information(release, counter, "Artist Name", "artist name")
                self.format_release_information(release, counter, "Release Date", "release date")
                self.format_release_information(release, counter, "Spotify Popularity", "spotify popularity")
                self.format_release_information(release, counter, "Custom Rating", "rating")
                
                # Add a spacer before the release
                self.frame_release_spacer = tkinter.Frame(self.frame_left_middle,
                                                          height=self.release_spacer_height,
                                                          bg=col_background)
                self.frame_release_spacer.pack(fill=tkinter.X)
                
                counter += 1

    
    def format_release_information(self, release, counter, label_name, label_value):
        '''Format the release information so that it displays nicely in the GUI rows.'''
        
        # Alternate background colour
        if counter % 2 == 0:
            col_background = self.col_release_info_light
        else:
            col_background = self.col_release_info_dark
        
        # Add a row for ease of formatting
        self.frame_release_info = tkinter.Frame(self.frame_release,
                                                bg=col_background)
        self.frame_release_info.pack()
        
        # Add the name of the field
        self.release_info_name = tkinter.Label(self.frame_release_info, 
                                               text=label_name,
                                               width=self.label_name_width,
                                               anchor=tkinter.W,
                                               bg=col_background)
        self.release_info_name.grid(column=0,
                                    row=0,
                                    padx=(self.padding_release_horizontal, 0),
                                    sticky=tkinter.W)
        
        # Add the value of the field
        self.release_info_value = tkinter.Label(self.frame_release_info, 
                                                text=release.get_release_information(label_value), 
                                                width=self.label_value_width, 
                                                anchor=tkinter.W,
                                                bg=col_background)
        self.release_info_value.grid(column=1,
                                     row=0,
                                     padx=(0, self.padding_release_horizontal),
                                     sticky=tkinter.W)
    
    
    def get_new_releases(self):   
        '''Get the raw JSON from the new releases Spotify API request, then convert it to a list for later use.'''
            
        # Make sure the limit and offset are valid
        if self.release_display_limit.get() < 1:
            self.release_display_limit.set(1)
        
        if self.new_release_offset.get() < 0:
            self.new_release_offset.set(0)
        
        # Send the request to Spotify
        self.new_releases_raw = self.spotify.get_new_releases(self.release_display_limit.get(), 
                                                              self.new_release_offset.get())
        
        # Convert the JSON reply to a list for use in other areas of the application
        self.new_releases_to_list()
        self.update_release_ratings()
        
        # Display the releases
        self.display_releases()
    

    def new_releases_to_list(self):
        '''Convert the JSON from Spotify to a list for use in other areas of this application.'''
        
        if self.new_releases_raw == None:
            return
        
        if self.list_of_releases == None:
            return
        elif len(self.list_of_releases) != 0:
            self.list_of_releases.clear()
        
        # For each release in the JSON content, extract the information and use it to create an instance of Release
        for item in self.new_releases_raw['albums']['items']:
            album_id = item['id']
            album_name = item['name']
            album_type = item['album_type']
            artist_id = item['artists'][0]['id']
            artist_name = item['artists'][0]['name']
            
            album_info = self.spotify.get_album_info(album_id)
            popularity = album_info['popularity']
            release_date = album_info['release_date']
            
            self.list_of_releases.append(Release.Release(album_id, 
                                                         album_name,
                                                         album_type,
                                                         artist_id,
                                                         artist_name,
                                                         popularity,
                                                         release_date))


    def open_ratings_file(self, file_path=""):
        '''Present the user with a browser to select a CSV file to open.'''
        
        if self.show_gui:
            # Display a file browsing window for the user to select a file
            file_path = tkinter.filedialog.askopenfilename(title="Open CSV file...",
                                                           initialdir="/",
                                                           filetypes=[("CSV files", "*.csv")])        
        
        if file_path == "":
            return
        
        self.csv_albums.clear()
        self.csv_artists.clear()
        self.csv_ratings.clear()
        
        # Open the file and parse the data
        with open(file_path, "r") as lookup_file:
            read_csv = csv.reader(lookup_file, delimiter=",")
            
            for row in read_csv:
                self.csv_albums.append(row[0])
                self.csv_artists.append(row[1])
                self.csv_ratings.append(row[2])
        
        # Apply the ratings to the release info
        self.update_release_ratings()


    def quit_application(self):
        '''Closes the GUI and ends the application.'''
        
        self.window_root.destroy()
        

    def reset_database(self):
        '''Reset the database attached to this GUI.'''
        
        self.acme_db.reset_new_releases()

    
    def save_new_releases_json(self, file_path=""):
        '''Save the Spotify API request to a JSON file.'''      
        
        # Make sure there is JSON content
        if self.new_releases_raw == None:
            return
        
        if self.show_gui:
            # Display a file browser for the user to select where to save the file and what to name it
            file_path = tkinter.filedialog.asksaveasfilename(title="Save file as...",
                                                             initialdir="/",
                                                             defaultextension=".json",
                                                             filetypes=(("JSON file","*.json"),
                                                                        ("All files","*.*")))

        if file_path == "":
            return
        
        # Save the JSON data to the specified file
        with open(file_path, "w") as output_file:
            output_file.write(json.dumps(self.new_releases_raw, indent=4, sort_keys=True))
                        
    
    def update_release_ratings(self):
        '''Update the custom ratings given to the releases via a CSV file.'''
        
        update_display = False
        
        # Make sure there's albums to check against
        if len(self.csv_albums) < 1:
            return
        
        # For each release, see if there's a rating that can be updated
        for release in self.list_of_releases:
            album_name = release.get_release_information("album name")
            artist_name = release.get_release_information("artist name")
            
            occurances = [i for i, x in enumerate(self.csv_albums) if x == album_name]
            
            if len(occurances) > 0:
                for index in occurances:
                    if self.csv_artists[index] == artist_name:
                        release.update_release_rating(self.csv_ratings[index])
                        update_display = True
        
        # If a rating was updated, display the release information again
        if update_display:
            self.display_releases()
    
    
    def view_newer_releases(self):
        '''Lower the offset to display more recent new releases.'''
        
        self.new_release_offset.set(self.new_release_offset.get() - self.release_display_limit.get())
        
        # Disable the 'newer' button when the offset can no longer be lowered
        if self.new_release_offset.get() <= 0:
            self.new_release_offset.set(0)
            
            if self.show_gui:
                self.button_view_newer.config(state=tkinter.DISABLED)
        
        self.get_new_releases()  
    
        
    def view_older_releases(self):
        '''Increase the offset to display less recent new releases'''
        
        self.new_release_offset.set(self.new_release_offset.get() + self.release_display_limit.get())
        
        # Enable the 'newer' button when the offset can be lowered
        if self.new_release_offset.get() > 0:
            if self.show_gui:
                self.button_view_newer.config(state=tkinter.NORMAL)
              
        self.get_new_releases()