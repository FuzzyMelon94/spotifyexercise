# Spotify Technical Exercise #

Develop a solution using your technology of choice that retrieves the Spotify New Releases resource https://developer.spotify.com/web-api/console/get-new-releases/, enriches the data with a �rating� field (e.g. 1,2,3,4,5) using a lookup file (name, rating) and publishes the current list to consumers.

## Getting Started ##

To run this program you will need to have Python 3.6 installed, then simply download these files and run the "RunApplication.py" file in the source folder. For more information, see below.

Please note, you will find a "SpotifyRatings.csv" file in the "Other Files" folder. You can use this to add the ratings to the application.

### Prerequisites ###

You will need to install the following to run this program:

- Python3.6
- The 'spotipy' package for Python3
- Tkinter if you're on a Mac
	
### Installation ###

Use the following instructions to get this application up and running on either Windows or MacOS.

#### Windows ####
1. Go to "Files to install > Windows"
2. Double click "python-3.6.2-amd64.exe" to start the installer
	- Check the "Add Python 3.6 to PATH" tickbox a the bottom of the installer window
	- Click "Install Now"
3. When the insatllation is complete, click "Close"
4. Navigate to the source folder containing "RunApplciation.py"
5. Make sure no files are selected
6. Press Ctrl+Shift and right click (make sure not to click on any files or folders)
7. Select "Open command window here" (or "PowerShell window") from the menu
8. In the command line, type "pip install spotipy"
9. Press Enter and let that install - if it's already installed it will say "Requirement already satisfied"
10. Close the command line window
11. double click "RunApplication.py"
	- A command line window will open, you should see "Starting GUI, please wait..." appear
	- A moment later the GUI should appear and you can use the application

#### MacOS ####
1. Go to "Files to install > Mac"
2. Double click on "python-3.6.2.pkg"
3. The installer will appear, follow through the prompts
4. When Python has been installed, open Terminal and type "python3 --version"
5. You should see "Python 3.6.2" - this confirms it was installed successfully
6. Enter "pip3 install spotipy" - this will install spotipy if not present already
	- If you already have it, you'll see messages saying "Requirement already satisfied"
7. Close the Terminal window
8. Go to "Files to install > Mac"
9. Double click on "ActiveTcl8.5.15.dmg"
10. In the window that appears, double click the package icon to install it
11. An installation window will appear, follow the prompts
12. Once installed, close the package window
13. Open the source folder containing "RunApplication.py"
14. Open Terminal
15. Type "python3 " (make sure to include the space, and don't press enter yet)
16. Click and drag the "RunApplication.py' file into the Terminal window
17. The file directory should appear - something like "python3 /Volumes/USB/SpotifyExercise/src/RunApplication.py"
18. Now press Enter
19. You should see the text "Starting the GUI, please wait..." appear
20. Shortly after the application window should appear

## Unit Tests ##

I built this application and created all my unit tests using the Eclipse IDE on Windows. You can find the test scripts in the "test" folder of the application source. 

## Design Decistions ##

The following is a short commentary regarding some of the choices I made through the development of this project.

### Python ###
I ultimately decided to use Python to create this application. The last time I used it was about 3 years ago, but I remember making a web application that allowed a user to log in and out of a website and display some content - all part of an assignment at uni. Originally I intended to make this application web based, or at least launch and be used through the users web browser, as I would be able to make use of the HTML and CSS I've picked over the past year at my current job, but I found this to be a bit of a time sink and decided to go for the desktop option. Looking back, I spent more time than I would have liked researching how to do things in Python, but overall it has been a fun and interesting exercise.

### Language Considerations ###
I initially considered PHP or Node.js, but after some research it became apparent these would be difficult to get working in an 'offline' mode as they're primarily server based, from my understanding. I also made a start on this project using JavaScript as I knew I'd be able to run it in any browser - I thought it would be a good way to ensure cross-platform compatibility. Unfortunately, after working for a few hours, I realised I wouldn't be able to easily connect with a database, and so I scrapped the project and started again using Python. After doing an hour or so of research, I knew I'd be able to do everything I needed, communicate with a database, import a lookup file, make HTTP requests and make a GUI, though I was still considering the HTML approach at this point. I also found there were a few different ways to package the Python file to get it to work on different platforms as a standalone program, running without requiring the user to first install Python - these gave me a little trouble.

### Type ###
I decided to use the 'album_type' for the 'type' field in the database. This is because type for release always seemed to be 'album', whereas the 'album_type' varied from 'album' to 'single'. I thought this would be better to use as it gave a little more information about the release.

### Date ###
I added the release date to the 'date' field of the database. I did this because it appears Spotify has some extra sorting on the releases returned by the new release API call. I noticed that whenever the returned result was updated the orders sometimes changed. I thought that adding a 'date' field would provide a way of sorting the database if necessary.

### Lookup File Cross-checking ###
The technical exercise specified an external lookup file could be used to add ratings to the releases using a name. I decided that name referred to the name of the album, as you could easily have more than one occurance of an artists name in the list of new releases. I also decided this wasn't enough information to make update the correct release with the specified rating, so I added the artist name to the lookup file. Now when added, the application first checks for the album name, if it finds a match it checks the artist name. If both names match the rating is applied, otherwise the application checks for another occurance of the album name. I implemented this because there were two albums named "Spotify Releases" in the list when I first started development, I thought this may not be an uncommon case.

## Authors ##

* Tom Brown