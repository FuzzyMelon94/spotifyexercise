'''
Created on 8 Sep. 2017

@author: Tom
'''
import unittest
from Spotify import Spotify

class TestSpotify(unittest.TestCase):
    
    # "What Lovers Do (feat. SZA)" - Maroon 5
    uri_what_lovers_do = "spotify:album:4DQ69vlzv8evjMG7oU9beE"
    id_what_lovers_do = "4DQ69vlzv8evjMG7oU9beE"
    
    
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.spotify = Spotify()
    
    def test_new_releases(self):
        new_releases = self.spotify.get_new_releases(1)
        self.assertNotEqual(new_releases, None, "Returns an object")
        
        
    def test_album_information(self):  
        album_info = self.spotify.get_album_info(self.id_what_lovers_do)
        album_uri = album_info['uri']
        self.assertEqual(album_uri, self.uri_what_lovers_do, "URIs don't match")
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()