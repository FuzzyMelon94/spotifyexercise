'''
Created on 8 Sep. 2017

@author: Tom
'''
import unittest
import sqlite3
from AcmeDatabase import AcmeDatabase

class Test(unittest.TestCase):

    database = AcmeDatabase("test.db")
    test_table_name = "test_table"
    

    def test_create_table(self):
        '''Create a table, then make sure it exists.'''
        
        self.database.connect()
        
        # Make sure the table isn't already there
        self.database.conn.execute("DROP TABLE IF EXISTS {}".format(self.test_table_name))
        
        self.database.create_table(self.test_table_name)
        
        query_string = """SELECT name FROM sqlite_master WHERE type='table' 
                          AND name='{}'""".format(self.test_table_name)
        cursor = self.database.conn.execute(query_string)
        self.assertTrue(cursor.fetchone(), "The table does not exist.")
        
        # Clean up afterwards
        self.database.conn.execute("DROP TABLE IF EXISTS {}".format(self.test_table_name))
        self.database.close()
        
        
    def test_drop_table(self):
        '''Drop a table, then make sure it doesn't exist.'''
        
        self.database.connect()
        
        # Make sure the table is there to begin with
        self.database.create_table(self.test_table_name)
        query_string = """SELECT name FROM sqlite_master WHERE type='table' 
                          AND name='{}'""".format(self.test_table_name)
        cursor = self.database.conn.execute(query_string)
        self.assertTrue(cursor.fetchone(), "The table does not exist before dropping.")
        
        # Drop the table
        self.database.drop_table(self.test_table_name)
        query_string = """SELECT name FROM sqlite_master WHERE type='table' 
                          AND name='{}'""".format(self.test_table_name)
        cursor = self.database.conn.execute(query_string)
        self.assertFalse(cursor.fetchone(), "The table still exists, it shouldn't.")
        
        self.database.close()
        

    def test_setup_new_releases(self):
        '''Create the new_releases table, then make sure it exists.'''
        
        self.database.setup_new_releases()
        
        self.database.connect()
        query_string = """SELECT name FROM sqlite_master WHERE type='table' 
                          AND name='{}'""".format("new_releases")
        cursor = self.database.conn.execute(query_string)
        self.assertTrue(cursor.fetchone(), "The new_releases table does not exist.")
        
        # Clean up afterwards
        self.database.drop_table("new_releases")
        self.database.close()
    
    
    def test_reset_new_releases(self):
        '''Set up the new_releases table with an entry, then 
           call reset and make sure the release is removed.'''
              
        # Make sure the table is set up
        self.database.setup_new_releases()
        self.database.connect()
        query_string = """SELECT name FROM sqlite_master WHERE type='table' 
                          AND name='{}'""".format("new_releases")
        cursor = self.database.conn.execute(query_string)
        self.assertTrue(cursor.fetchone(), "The new_releases table does not exist.")
        self.database.close()
        
        # Make sure the table has some data
        self.database.add_new_releases("abc123", 'John Doe', "album", "2017-09-08")
        test_release = self.database.find_new_release("abc123")
        self.assertEqual(test_release["id"], "abc123", "No entry in new_releases.")
        
        # Reset the new_releases table
        self.database.reset_new_releases()
        test_release = self.database.find_new_release("abc123")
        self.assertEqual(test_release["id"], "false", "The entry is still present in new_releases.")
        
        # Clean up afterwards
        self.database.connect()
        self.database.drop_table("new_releases")
        self.database.close()
        
        
    def test_add_new_releases(self):
        '''Add a release to the database, then confirm.'''
        
        self.database.reset_new_releases()
        self.database.add_new_releases("abc123", 'John Doe', "album", "2017-09-08")
        test_release = self.database.find_new_release("abc123")
        self.assertEqual(test_release["id"], "abc123", "No entry in new_releases.")
        
        # Clean up afterwards
        self.database.connect()
        self.database.drop_table("new_releases")
        self.database.close()


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()