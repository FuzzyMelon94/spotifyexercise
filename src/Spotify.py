'''
Created on 4 Sep. 2017

@author: Tom
'''

import spotipy
from spotipy import oauth2


class Spotify(object):
    '''Communicates with the Spotify API.'''
    
    # The keys required for the applications authorisation with Spotify
    CLIENT_ID = 'b2efa535462f453dadb0775249341b36'
    CLIENT_SECRET = 'f3111a7c06f64f63b0352eefb43c753f'

    spotify = None

    def __init__(self):
        '''Authorises the application for use with Spotify'''
        
        token = oauth2.SpotifyClientCredentials(client_id=self.CLIENT_ID, 
                                                client_secret=self.CLIENT_SECRET)
        cache_token = token.get_access_token()
        self.spotify = spotipy.Spotify(cache_token)
    
    
    def get_album_info(self, album_id):
        '''Returns the popularity of an album based on the ID'''

        return self.spotify.album(album_id)
    
    
    def get_new_releases(self, item_limit=5, item_offset=0):
        '''Returns the raw JSON produced by the API query for the new releases, 
        limited and offset by the parameters.'''
           
        return self.spotify.new_releases(limit=item_limit, offset=item_offset)